import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import { register } from '../class/post';

@Injectable({
  providedIn: 'root'
})
export class EndpointsService {

  constructor( private httpClient : HttpClient ) { }

  httpOptions = {
    headers: new HttpHeaders({
      enctype: "multipart/form-data"
    })
};

postUrl_1: string = "http://localhost:5000/upload/"; //for poc

postUrl_2: string = "http://localhost:5000/register/"; //for assignment

postUrl_3: string = "http://localhost:5000/update/"; //for assignment

addPost_1(postD) {
  console.log(postD);
  return this.httpClient.post(this.postUrl_1, postD, this.httpOptions);  
} // for poc



register(postD) {
  console.log(postD);
  return this.httpClient.post(this.postUrl_2, postD, this.httpOptions);  
} // for assignment

update(postD) {
  console.log(postD);
  return this.httpClient.post(this.postUrl_3, postD, this.httpOptions);  
} // for assignment


}
