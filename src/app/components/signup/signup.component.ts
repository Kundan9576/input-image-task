import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { register } from '../../class/post';
import { EndpointsService } from '../../services/endpoints.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor( private _router: Router, private servefile : EndpointsService ) { }

  ngOnInit() {
  }

  @ViewChild('email', { static: false })
  email_input: any;
  @ViewChild('first_name', { static: false })
  first_input: any;
  @ViewChild('second_name', { static: false })
   second_input: any;
  @ViewChild('password', { static: false })
   age_input: any;
  @ViewChild('birth', { static: false })
  birth_input: any;

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  submit_on( ){
    if(
      this.email_input.nativeElement.value =="" ||
      this.first_input.nativeElement.value =="" ||
      this.second_input.nativeElement.value =="" ||
      this.age_input.nativeElement.value =="" ||
      this.birth_input.nativeElement.value =="" 
    ){
      this.correct = false;
    }
    else{
      console.log("true");
      this.correct = true;
    }
  }

  chechEmail( checkemail){
  let temp = checkemail.split("@");
  let flagmail = 0;
  if(temp.length==2){
    var temp2=temp[1].split(".");
    if(temp2.length==2){ 
      flagmail=1; 
      this.submit_on();     
    }
    else{
      flagmail=0;
      this.email_input.nativeElement.value = "";
      this.email_input.nativeElement.placeholder = "Please enter correct E-mail";
      this.correct = false;
    }
  }
  else{
   flagmail = 0;
   this.email_input.nativeElement.value = "";
   this.email_input.nativeElement.placeholder = "Please enter correct E-mail";
   this.correct = false;
  }
 
  }


  check_first( value, str ){
    if( this.isNumeric(value) ){
      if(str == "Name"){
        this.second_input.nativeElement.value = "";
        this.second_input.nativeElement.placeholder = "Name does not contain number";
        this.correct = false;
      }
      else{
        this.submit_on();
      }      
    }
    else{
      this.submit_on();
    }

  }
   
  correct:boolean = false;
  registerData: register;
  register_results;


  dataGet( first_name, second_name, password, birth, email ){
    if( first_name=="" || second_name ==""|| password =="" || birth == "" || email =="" ){
      window.alert("Wrong Input");
    }
    else{

      this.fd = new FormData();
      this.fd.append('image', this.selectedfile, this.selectedfile.name);
      this.fd.append('id', first_name);
      this.fd.append('name', second_name);
      this.fd.append('phone', birth);
      this.fd.append('password', password);
      this.fd.append('email', email);

      this.servefile.register(this.fd).subscribe( res => {
        this.register_results = res;
        if(this.register_results.status == "Success"){
          window.alert("Response submitted Successfully.");
        }
        else if(this.register_results.status == "User Already Exists."){
          window.alert("User Already Exists");
        }
        else if(this.register_results.status == "Invalid Input"){
          window.alert("Invalid Input");
        }
        else{
          window.alert("Something went wrong");
        }
      })
      
    }
  }


  dataUpdate( first_name, second_name, password, birth, email ){
    if( first_name=="" || second_name ==""|| password =="" || birth == "" || email =="" ){
      window.alert("Wrong Input");
    }
    else{

      this.fd = new FormData();
      this.fd.append('image', this.selectedfile, this.selectedfile.name);
      this.fd.append('id', first_name);
      this.fd.append('name', second_name);
      this.fd.append('phone', birth);
      this.fd.append('password', password);
      this.fd.append('email', email);

      this.servefile.update(this.fd).subscribe( res => {
        this.register_results = res;
        if(this.register_results.status == "Success"){
          window.alert("Response submitted Successfully.");
        }
        else if(this.register_results.status == "No User Exists."){
          window.alert("No User Exists.");
        }
        else if(this.register_results.status == "Invalid Input"){
          window.alert("Invalid Input");
        }
        else{
          window.alert("Something went wrong");
        }
      })
      
    }
  }

  imgURL;
  url:string = "";
  selectedfile;
  fd;
  preview(files) {
    if (files.length == 0)
      return;
 
    var mimeType = files[0].type;
    this.selectedfile = files[0];
    if (mimeType.match(/image\/*/) == null) {
      window.alert("Give only image file.");
      return;
    }
    // console.log(files[0].size);
    if(files[0].size/(1024*1024) <= 2) 
    {
      var reader = new FileReader();
      reader.readAsDataURL(files[0]); 
      reader.onload = (_event) => { 
      this.imgURL = reader.result;  
      this.url = this.imgURL;
      this.correct = true;
      }
    }
    else{
      window.alert("Please provide file size of less than 2MB. Please provide image which size is less than 2 MB.");
    }
   
  }
  
  reg_start: boolean = true;
  register(){
   this.reg_start = true;
  }

  update(){
    this.reg_start = false;
  }

}
