import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EndpointsService } from '../../services/endpoints.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor( private _router: Router, private servefile : EndpointsService ) { }

  ngOnInit() {
    
  }

  selectedfile;
  fd;
  fileselect( event ){
    this.selectedfile = event.target.files[0];
  }
  
  results;
  testapi(){
    this.fd = new FormData();
    this.fd.append('image', this.selectedfile, this.selectedfile.name);
    // this.fd.append('name', 'kundan');
  
    this.servefile.addPost_1(this.fd).subscribe( response => {
      this.results = response;
      console.log(this.results);    
      });
  
    }

}
